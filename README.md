# perl-Net-Ping

Net::Ping module contains methods to test the reachability of remote hosts on
a network.